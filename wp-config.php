<?php
/** 
 * A WordPress fő konfigurációs állománya
 *
 * Ebben a fájlban a következő beállításokat lehet megtenni: MySQL beállítások
 * tábla előtagok, titkos kulcsok, a WordPress nyelve, és ABSPATH.
 * További információ a fájl lehetséges opcióiról angolul itt található:
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php} 
 *  A MySQL beállításokat a szolgáltatónktól kell kérni.
 *
 * Ebből a fájlból készül el a telepítési folyamat közben a wp-config.php
 * állomány. Nem kötelező a webes telepítés használata, elegendő átnevezni 
 * "wp-config.php" névre, és kitölteni az értékeket.
 *
 * @package WordPress
 */

// ** MySQL beállítások - Ezeket a szolgálatótól lehet beszerezni ** //
/** Adatbázis neve */
define('DB_NAME', 'Tandofer');

/** MySQL felhasználónév */
define('DB_USER', 'Tandofer');

/** MySQL jelszó. */
define('DB_PASSWORD', 'Tandoferkft6000');

/** MySQL  kiszolgáló neve */
define('DB_HOST', 'localhost');

/** Az adatbázis karakter kódolása */
define('DB_CHARSET', 'utf8mb4');

/** Az adatbázis egybevetése */
define('DB_COLLATE', '');

/**#@+
 * Bejelentkezést tikosító kulcsok
 *
 * Változtassuk meg a lenti konstansok értékét egy-egy tetszóleges mondatra.
 * Generálhatunk is ilyen kulcsokat a {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org titkos kulcs szolgáltatásával}
 * Ezeknek a kulcsoknak a módosításával bármikor kiléptethető az összes bejelentkezett felhasználó az oldalról. 
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'rf(4nz[%(.P-QCL,{{RCZm<!;PjugYexR.,}Ny#hg3W.DbntP8cg!SOb~;#J-Lps');
define('SECURE_AUTH_KEY', '^>P5>u*!>!U~L[[v]!}4{`iB@uf4<`;ck&M}-#6LD79q00OIqHeqQ1qq>c&]~u|5');
define('LOGGED_IN_KEY', 'B%Vp+NJ_ K^@sJ|2!#!&CY6|k0L+/=^k/RS>pb(chGukT}5 Ud#K^B?Q<o><QEmw');
define('NONCE_KEY', 'forz|Uor8(yH<c$65Uzqs{^g:kFYN45g+8C:.k4nj/&*siAL!vxzC]Qv=%hzsQpw');
define('AUTH_SALT',        'tKu-&<#}~LaVq9!ZO*e$_u!MaiqkQxc8BHdy|L.Lq|#j@oSDho*!HoC<2]u %[Nx');
define('SECURE_AUTH_SALT', '@&o7i66dA2Uy#v(I@/!a`Y!+5D+54s&GQM?os}N}UJ(FH84JRdXlKxx<8U{<PX #');
define('LOGGED_IN_SALT',   'b@>d:~O[.rUG]Y)RN>U+c_p`h<Bn66}MJG2~J]<<llKLS@alr%Y[dv6N-vPBv;Xp');
define('NONCE_SALT',       'Hf93)y87J]b+Z*ih.p}`.>1^jO2J4=&yR8<Qse~I@D%U:Cu0bY6I6#9_O#onef_l');

/**#@-*/

/**
 * WordPress-adatbázis tábla előtag.
 *
 * Több blogot is telepíthetünk egy adatbázisba, ha valamennyinek egyedi
 * előtagot adunk. Csak számokat, betűket és alulvonásokat adhatunk meg.
 */
$table_prefix  = 'wp_';

/**
 * Fejlesztőknek: WordPress hibakereső mód.
 *
 * Engedélyezzük ezt a megjegyzések megjelenítéséhez a fejlesztés során. 
 * Erősen ajánlott, hogy a bővítmény- és sablonfejlesztők használják a WP_DEBUG
 * konstansot.
 */
define('WP_DEBUG', false);

/* Ennyi volt, kellemes blogolást! */

/** A WordPress könyvtár abszolút elérési útja. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Betöltjük a WordPress változókat és szükséges fájlokat. */
require_once(ABSPATH . 'wp-settings.php');
