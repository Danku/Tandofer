<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Sydney
 */
$webSite = site_url();
get_header(); ?>

	<div id="primary" class="content-area fullwidth">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header"><br>
					<h1 class="page-title"><?php _e( 'A keresett oldal nem található!', 'sydney' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<img style="height: 300px; width:auto;" src="<?php echo $webSite;?>/wp-content/uploads/2018/08/404.png" alt="404">
					<br><br>
					<?php //get_search_form(); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
