<?php
/*
Plugin Name: kosar_plugin
Plugin URI: http://danku.hu
Description: kosár termék vásárlás
Version: 0.1
Author: Danku Attila Zsolt
Author URI: Danku Attila Zsolt
License: MIT
Text Domain: Termék listázó
*/
//[termek_get] termékeket össze szedi
//[termek_send] Termékeket küldi megrendelésre
//[basket_mail] Email küldés a megrendelő lapról
//[vissza_szofvter]
//[vissza_hardver]
//[vissza_szolgaltatasok]
/*<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/my_custom_css.css" type="text/css" media="screen" /> */


function add_my_scripts() {
   /* wp_enqueue_script (
		'my-jquery',
		plugins_url( 'kosar_plugin/js/jquery.js' ),
		array(),
		date('YmdHis')
    );*/
    
    wp_enqueue_script (
		'my-mainjs',
		plugins_url( 'kosar_plugin/js/main.js' ),
		array('jquery'),
        date('YmdHis'),
        false
	);

	// Egyéni stíluslap.
	wp_enqueue_style (
		'myStyle',
		plugins_url( 'kosar_plugin/css/my.css' ),
		array(),
		date('YmdHis')
    );    

}
add_action( 'wp_enqueue_scripts', 'add_my_scripts' );


//kosár
function page_short_code(){
    ob_start();
    include 'inc/tget_session.php';
    $content = ob_get_contents();
    ob_clean();
    return $content;
}
add_shortcode('termek_get', 'page_short_code');

//Termék megrendel
function page_id_short_code(){
    ob_start();
    include 'inc/tsend_session.php';
    $content = ob_get_contents();
    ob_clean();
    return $content;
}
add_shortcode('termek_send', 'page_id_short_code');

//Vissza a termékhez
function visszaTermekhezFunc(){
    ob_start();
    include 'inc/vissza.php';
    $content = ob_get_contents();
    ob_clean();
    return $content;
}
add_shortcode('vissza_termekhez', 'visszaTermekhezFunc');

function kosarMailFunc(){
    ob_start();
    //if(is_user_logged_in()){
        include 'inc/session_mail_basket.php';
    //}
    $content=ob_get_contents();
    ob_clean();
    return $content;
}
add_shortcode('basket_mail', 'kosarMailFunc');



?>