<?php

global $wpdb;
$logged = is_user_logged_in();
if( ! $logged){
    echo "
    <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
        <div class='elementor-widget-container'>
        <div class='elementor-alert elementor-alert-danger' role='alert'>
        <span class='elementor-alert-title'>
            Kérjük, jelentkezzen be!
        </span>
        <span class='elementor-alert-description'>
            <a href='index.php?p=13'>Bejelentkezés / Regisztráció.</a>
        </span>
        <button type='button' class='elementor-alert-dismiss'>
        <span aria-hidden='true'>&times;</span>
        <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
        </button>
        </div>
        </div>
    </div><br>
    ";
}

// $_GET vizsgáltata sidebarrol kaptunk-e adatot.
if(isset($_GET['add'])){
    $ulrtTmb = $_GET;
    foreach ($ulrtTmb as $value) {
        $product = $value;
    }
    $megrendel = $product;
    $user_id = get_current_user_id();
    basket_create($user_id, $megrendel);
    $ulrtTmb = "";
}
//törlés
if(isset($_GET['delete'])){
    $table='basket_list';
    $id=$_GET['delete_id'];
    $basket_id=$_GET['basket_id'];
   // echo "$id  $basket_id <br>";
    //melyik felhasználó kosár és melyik terméket
    $where=[
        'product_id' => $id,
        'basket_id' => $basket_id
    ];
    $success = $wpdb->delete($table, $where);
    if($success == true) {
        echo "<h3 id='2'>Sikeres törlés!</h3>"; 
    }
    $ulrtTmb = "";
}
//kosár ürítése
function deleteBasketList($basketId){
    global $wpdb;
    $table='basket_list';
    $where=[
        'basket_id' => $basketId
    ];
    $succes = $wpdb->delete($table, $where);
    $ulrtTmb = "";
    if($succes){
        //echo "lista törölve";
    }
}
//echo "$megrendel<br>";
if(isset($_POST['oldal_id'])){
    global $wpdb;
    $megrendel= $_POST['oldal_id'];
    $table='products';
    $row=$wpdb->get_row(
        "SELECT * FROM $table WHERE p_page_id = $megrendel"
    );
    /*echo '<pre>';
    print_r($row);
    echo '</pre>';*/
    //echo $row->p_name." ".$row->p_page_id;
    $p_name=$row->p_name;
    $p_page_id=$row->p_page_id;
    $user_id=get_current_user_id();
    basket_create($user_id, $megrendel);
    
}
//Kosár létrehozása
function basket_create($user_id, $megrendel){
    //Ha be van jelentkezve csak akkor adhat terméket a megrendelő laphoz
    if($user_id > 0){
    global $wpdb;
    $table='basket';
    $row=$wpdb->get_row(
        "SELECT * FROM $table WHERE user_id=$user_id"
    );
    if($row==false){
        //adat a létrehozáshoz
        $data=[
            'user_id' => $user_id
        ];
        $formats=[ '%d' ];
        //kosár beszúrása
        $wpdb->insert($table, $data, $formats);
        //echo "kosár létrehozva $wpdb->insert_id id-val";
    }
    $row=$wpdb->get_row(
        "SELECT * FROM $table WHERE user_id=$user_id"
    );
    $basket_id=$row->id;   
    basket_put($basket_id, $megrendel);

} 
}
//Termék kosárba helyezése ha még nem szerepel
function basket_put($basket_id, $megrendel){
    global $wpdb;
    $table='basket_list';
    $row=$wpdb->get_row(
        "SELECT * FROM $table WHERE
         basket_id=$basket_id AND product_id=$megrendel"
    );
    //Ha a kosrában nincs még akkor hozzáadhatjuk
    if($row==false){
        $data=[
            'product_id' => $megrendel,
            'basket_id' => $basket_id
        ];
        $formats = [ '%d', '%d' ];
        $wpdb->insert($table, $data, $formats);
    } else {
        $row=$wpdb->get_row(
            "SELECT * FROM products WHERE p_page_id=$megrendel"
        );
        $row=$row->p_name;
        echo "<h3 id='1'>Ez a termék már szerepel a megrendelő lapon:<br> ( $row )</h3><br>";
    }
    $ulrtTmb = "";
}


//Termékek kiíratása és törlése a kosárból
$user_id=get_current_user_id();
basket_products_listing($user_id);
function basket_products_listing($user_id){
    global $wpdb;
    $logged = is_user_logged_in();
  //minden törlés funkció
if(isset($_GET['allDelete'])){
    $table='basket_list';
    $id=$_GET['all'];
    $where=[
        'basket_id' => $id
    ];
    $success=$wpdb->delete($table, $where);
    if($success==true){
        echo "<h3 id='3'>Minden törölve!</h3>"; 
    }
    $ulrtTmb = "";
}
    $table='basket';
    $basket_id=$wpdb->get_row(
        "SELECT * FROM $table WHERE user_id=$user_id"
    );
    $basket_id= $basket_id->id;
    $table='basket_list';
    $productList=$wpdb->get_results(
        "SELECT * FROM $table WHERE basket_id=$basket_id"
    );
    if($logged){
    if($productList==null){
        echo "
            <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
				<div class='elementor-widget-container'>
                <div class='elementor-alert elementor-alert-danger' role='alert'>
                <span class='elementor-alert-title'>
                    Nincs kiválasztott termék!
                </span>
                <span class='elementor-alert-description'>
                    <a href='index.php?p=822'>Vissza a termékekhez.</a>
                </span>
                <button type='button' class='elementor-alert-dismiss'>
                <span aria-hidden='true'>&times;</span>
                <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
				</button>
                </div>
				</div>
			</div>
            ";
    } else {

        if(!isset($_POST['send-submitted'])){//muszáj az alert ablak miatt....

    //kosár tartalom kiíratása táblázatba
    ?><br>
    <div id="table">
    <table class="no-border" >
    <thead class="no-border" >
      <tr class="no-border">
        <th class="no-border nev">Terméknév</th>
        <!--<th class="no-border leiras leirCim displaymobile"></th>-->
        <th class="no-border torol">Törlés         
        </th>
      </tr>
    </thead>
    <tbody class="no-border">
        
    <?php
    $table='products';
    foreach ($productList as $value) {
        $product = $value->product_id;
        $productName=$wpdb->get_row(
            "SELECT * FROM $table WHERE p_page_id=$value->product_id"
        );
        $pName=$productName->p_name;
        $id=$productName->p_page_id;
        $descripton=$productName->p_description;             
        ?>
        <tr class="no-border"><th class="no-border nev">
        <b><a href='index.php?p=<?php echo $id; ?>'><?php echo $pName;?></a> </b>
        </th>
        <!--<td class="no-border leiras displaymobile"></td>-->
        <td class="no-border torol">
        <form method="get">
        <input type="hidden" name="delete_id" value="<?php echo $id; ?>">
        <input type="hidden" name="basket_id" value="<?php echo $basket_id;?>">
        <button type="submit" name="delete" class="delete" ><i class="fa fa-close"></i></button>
        </form> 
        </td></tr>       
        <?php
    }   //minden törlés gomb 
   ?>
    </tbody>
    </table><br>
    <table class="displaymobile">
        <tr>
            <!--<th class="no-border vissza">
            <input type="button" value="<< vissza a termékekhez"
                onclick="window.location.href='index.php?p=822'" 
             />  
            </th>-->
            <th class="no-border" style="border:none;">
                <form method="get" class="allDelete">    
                    <input type="hidden" name="all" value="<?php echo $basket_id;?>">   
                    <input type="submit" name="allDelete" value="Összes törlése">
                </form>            
            </th>
        </tr>
    </table>
    <!--<div class="display">
    <input type="button" value="<< vissza a termékekhez" class="mobileButton"
                onclick="window.location.href='index.php?p=822'" 
             /> 
    </div>-->
    </div>

    <?php  
            }
        }    
    }
}