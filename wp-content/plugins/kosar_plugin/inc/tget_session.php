<?php
session_start();
$session_id = session_id();
//print_r($_SESSION);

date_default_timezone_set('Europe/Budapest');
oldSessionDeleteFunc();

$session_basket_id = createSessionDatabase($session_id);

if(isset($_GET['reCAPTCHA'])){
    deleteBasketAndClearList($session_id, $session_basket_id);
    echo "
           <div style='max-width : 500px;' data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
               <div class='elementor-widget-container'>
               <div class='elementor-alert elementor-alert-danger' role='alert'>
               <span class='elementor-alert-title'>
                   Köszönjük, hogy felvette velünk a kapcsolatot.
               </span>
               <span class='elementor-alert-description'>
                   Üzenet elküldve.
               </span>
               <button type='button' class='elementor-alert-dismiss'>
               <span aria-hidden='true'>&times;</span>
               <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
               </button>
               </div>
               </div>
           </div><br>
           ";
}

if(isset($_GET['allDel'])){
    basketListAllDelete($session_basket_id);
}

if(isset($_GET['del'])){
    $productId = $_GET['del'];
    productDelete($productId, $session_basket_id);
}

if(isset($_GET['add']) || isset($_GET['addButton'])){
    if(isset($_GET['add'])){
        $productId = $_GET['add'];
    } else {
        $productId = $_GET['addButton'];
    }
    addBasketListGet($session_basket_id, $productId);
}

//addBasketList($session_basket_id);
//deleteBasketAndClearLict($session_id, $session_basket_id);
$results = productListInBasket($session_basket_id);

if( ! empty($results)){
?>
<div>
    <table style="margin-bottom:10px;">
        <thead>
            <tr class="no-border">
                <th class="no-border nev">Terméknév</th>
                <th class="no-border torol">Törlés</th>
            </tr>
        </thead>
        <tbody class="no-border">
        <?php 
            foreach ($results as $value) {
                $productId = $value->p_product;
                $productName = productName($productId);            
        ?>
            <tr class="no-border">
                <td class="no-border nev">
                    <?php echo $productName;?>
                </td>
                <td class="no-border torol">
                        <a href="<?php echo site_url();?>/megrendelo-lap/?del=<?php echo $productId;?>"
                         class="linkButton"><i class="fa fa-close"></i></a>        
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <form method='get' class="allDelete" action="<?php echo site_url();?>/megrendelo-lap/">
        <input type="hidden" name="allDel" value="sdjbgfil7895asugdfoias4fsdfs867DankusafdasfAttilafsagadhgZsolt3123fsaf2dsafdas1é381t2rgdsfq23eq">
        <Button class="mobileButton">ÖSSZES TŐRLÉSE</Button>
    </form>
</div>

<?php
}

function createSessionDatabase($session_id){
    global $wpdb;
    $table = 'session_basket';
    $results = $wpdb->get_row(
        "SELECT * FROM $table WHERE session_basket_id = '$session_id' "
    );

    //ha nincs még ilyen azonosító létrehozza 
    if( $results == false ){
        $data = [
            "session_basket_id" => $session_id
        ];
        $format = [ "%s" ];
        $insert = $wpdb->insert($table, $data, $format);
        $session_basket_id = $wpdb->insert_id;
    } else {
        $session_basket_id = $results->id;
    }
    return $session_basket_id;
}

function addBasketList($session_basket_id){
    /*global $wpdb;
    $table = "session_basket_list";
    $page_id = $_SESSION['page_id'];
    $row = $wpdb->get_row(
        "SELECT * FROM $table WHERE session_basket_id = $session_basket_id AND p_product = $page_id"
    );
    //print_r($row);
    if($row == false && $page_id != 0 ){
        $data = [
            'session_basket_id' => $session_basket_id,
            'p_product' => $page_id
        ];
        $format = [ '%d', '%d' ];
        $insert = $wpdb->insert( $table, $data, $format);
        if($insert){
           $_SESSION['page_id'] = 0;
        }
    }    */
}

function deleteBasketAndClearList($session_id, $session_basket_id){
    global $wpdb;
    //a kosár tartalmát törli
    $table = "session_basket_list";    
    $where = [ 'session_basket_id' => $session_basket_id ];
    $delete = $wpdb->delete($table, $where);
    if($delete){
        //echo "success";
    }
    //kosarat törli
    $table = "session_basket";
    $where = [ 'id' => $session_basket_id ];
    $delete = $wpdb->delete($table,$where);
    if($delete){
        //echo "succes";
    }
    return session_destroy();
}

function productListInBasket($session_basket_id){
    global $wpdb;
    $table = "session_basket_list";
    $results = $wpdb->get_results(
        "SELECT * FROM $table WHERE session_basket_id = $session_basket_id"
    );
    return $results;
}

function productName($productId){
    global $wpdb;
    $table = "products";
    $row = $wpdb->get_row(
        "SELECT * FROM $table WHERE p_page_id = $productId"
    );
    return $row->p_name;
}

function productDelete($productId, $session_basket_id){
    global $wpdb;
    $table = "session_basket_list";
    $row = $wpdb->get_row(
        "SELECT * FROM $table WHERE session_basket_id = $session_basket_id AND p_product = $productId"
    );  
    $id = $row->id;
    $where = [ 'id' => $id ];
    $delete = $wpdb->delete($table, $where);
    if($delete){
        //echo "success";
    }
}

function basketListAllDelete($session_basket_id){
    global $wpdb;
    $table = "session_basket_list";
    $where = [ 'session_basket_id' => $session_basket_id ];
    $delete = $wpdb->delete($table, $where);
    if($delete){
        //echo "success";
    }
}

function addBasketListGet($session_basket_id, $productId){
    global $wpdb;
    $table = "session_basket_list";
    $row = $wpdb->get_row(
        "SELECT * FROM $table WHERE session_basket_id = $session_basket_id AND p_product = $productId"
    );
   
    if($row == false){
        $data = [
            'session_basket_id' => $session_basket_id,
            'p_product' => $productId,
        ];
        $format = [ '%d', '%d' ];
        $insert = $wpdb->insert($table, $data, $format);

        if($insert){
            $date = date('Y-m-d h:i:s');
            $table = "session_basket";
            $data = [
                'edit' => $date
            ];
            //echo $date;
            $where = [ 'id' => $session_basket_id ];

            $update = $wpdb->update($table, $data, $where);
        }
    }
}

function oldSessionDeleteFunc(){
    //$d = strtotime(" -1 Months");
    //$date = date('Y-m-d h:i:s', $d);
    $date = date('Y-m-d h:i:s');
    global $wpdb;
    $table = "session_basket";
    $results = $wpdb->get_results(
        "SELECT * FROM $table"
    );

    foreach ($results as $value) {
        $edit = $value->edit;
        $id = $value->id;

        $diff = date_diff(date_create ($edit), date_create ($date));
        $diff = $diff->format ('%a');
        //echo $date;
        if($diff >= 30){
        $where = [ 'id' => $id ];
        //print_r($where);
        $delete = $wpdb->delete($table, $where);
        if($delete){
            //echo "success";
            }
      
        $table = "session_basket_list";
        $where = [ 'session_basket_id' => $id ];

        $delete = $wpdb->delete($table, $where);
        if($delete){
            //echo "sufasf";
            }
            //print_r($where);
        }      
    }        
}

?>
