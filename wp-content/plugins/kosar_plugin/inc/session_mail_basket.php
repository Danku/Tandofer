<?php 
session_start();
$session_id = session_id();
$webSite = site_url();

$session_basket_id = basketIdFunc($session_id);
$line = basketListFunc($session_basket_id);

if(isset($_POST['send-submitted'])){
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    //Secret key
    $privatekey = "6Lfhb2YUAAAAAMaySQxz_7csBUYmLj1kgCOwDi1i";

    $response = file_get_contents($url."?secret=".$privatekey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $data = json_decode($response);

    if(isset($data->success) AND $data->success==true){
        //true
        emailSendFunc($session_basket_id, $session_id, $line);
        //header("Location: $webSite/megrendelo-lap/?reCAPTCHA=FIsdfASUBBGFIasdfsadfsAFSAKDFGASIFGFDHÉsSOIHfsaÉASsafIfsfUGDVsSHVsadfÉDAsadfasfSGFVÉAU");
    } else {
        echo "
            <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
				<div class='elementor-widget-container'>
                <div class='elementor-alert elementor-alert-danger' role='alert'>
                <span class='elementor-alert-title'>
                    Kérjük, jelölje meg a \"Nem vagyok robot\" ablakot!
                </span>
                <span class='elementor-alert-description'>
                    reCAPTCHA hiba lépett fel. Kérjük ismételje meg újra.
                </span>
                <button type='button' class='elementor-alert-dismiss'>
                <span aria-hidden='true'>&times;</span>
                <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
				</button>
                </div>
				</div>
			</div><br>
            ";
    }
}

//Email form
if($line !=""){
?>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<div id="email-form" data-ng-app="">
    <form action="<?php echo site_url()?>/megrendelo-lap/" method="post" class="email">  
    <label for='send-name' class="label">Név: <span class="csillag">*</span></label><br>
    <input type="text" name="send-name" size="30" pattern="^[a-zA-ZáéíóöőüűúÁÉÍÓÖŐÜŰÚ ]*$"
    required ng-model="sendName" placeholder="Teljes név" class="email"><br>

    <label for='send-phone'>Telefonszám: <span class="csillag">*</span></label><br>
    <input type="text" name="send-phone" size="30" pattern="^[0-9 +/]*$" value=""
    required ng-model="sendPhone" placeholder="+36/xx xxx xx xx" class="email phone"/><br>

    <label for='send-mail'>Email cím: <span class="csillag">*</span></label> <br>
    <input type="email" name="send-mail" size="30" 
    required data-ng-model="sendEmail" placeholder="@Example.com"
        class="email"/><br>

    <label for='send-message'>Üzenet: <span class="csillag"></span></label> <br>
<textarea rows="10" cols="10" name="send-message" id="message_id" maxlength="1000">
Tisztelt Hölgyem / Uram!






Kiválaszott termékeim: <?php
echo $line;
?>


Üdvözlettel: <?php
echo "
{{sendName}}
{{sendPhone}}
{{sendEmail}}
"; ?>
</textarea><br>
        <div class="g-recaptcha" data-sitekey="6Lfhb2YUAAAAAEojPCav9Rr6A5TZxbmhlYqBlSbW"></div>
    <span>
    <input type="checkbox" name="check" id="check" value="1" required >
    <label for="check" class="check">Hozzájárulok, hogy </label> kapcsolattartás céljából a szolgáltató az általam megadott adatokat felhasználhatja. 
    <a href="index.php?p=1031">Adatkezelési tájékoztató.</a>
    </span>
    <br><br>
    <input type="submit" class="submitted mobileButton" name="send-submitted" value="Küldés"/>
    <!--<button type="submit" class="submitted" name="send-submitted"><i class="fa fa-envelope-o">  KÜLDÉS</i></button>-->
    </form>
</div>
<?php
} else {
    echo "
    <div style='max-width : 500px;' data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
        <div class='elementor-widget-container'>
        <div class='elementor-alert elementor-alert-danger' role='alert'>
        <span class='elementor-alert-title'>
            Nincs kiválasztott termék!
        </span>
        <span class='elementor-alert-description'>
            <a href='index.php?p=822'> << Vissza a termékekhez</a>
        </span>
        <button type='button' class='elementor-alert-dismiss'>
        <span aria-hidden='true'>&times;</span>
        <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
        </button>
        </div>
        </div>
    </div>
    ";
}
///////////////////////////////////
function emailSendFunc($session_basket_id, $session_id, $line){
       //átadjuk az adatokat és megtisztítjuk
       //email vizsgálata
       
       $message    = esc_textarea($_POST["send-message"]);
       $subject    = 'Ajánálatkérés';
       $name       = sanitize_text_field( $_POST["send-name"] );
       $email      = sanitize_email( $_POST["send-mail"] );
       $phone      = sanitize_text_field( $_POST["send-phone"] );
       // lekérjük az adminisztrátor email címét
       $to = get_option('admin_email');

       $headers = "Form: $name <$email>"."\r\n";
       
       // Feldolgozza az email küldést
       $ok=wp_mail($to, $subject, $message, $headers);
       if($ok){
           answerEmail($email, $name, $line);
           header("Location: $webSite/megrendelo-lap/?reCAPTCHA=FIsdfASUBBGFIasdfsadfsAFSAKDFGASIFGFDHÉsSOIHfsaÉASsafIfsfUGDVsSHVsadfÉDAsadfasfSGFVÉAU");
       } else {
           echo "
           <div style='max-width : 500px;' data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
               <div class='elementor-widget-container'>
               <div class='elementor-alert elementor-alert-danger' role='alert'>
               <span class='elementor-alert-title'>
                   Váratlan hiba lépett fel. Kérjük ismételje meg újra.
               </span>
               <span class='elementor-alert-description'>
                  
               </span>
               <button type='button' class='elementor-alert-dismiss'>
               <span aria-hidden='true'>&times;</span>
               <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
               </button>
               </div>
               </div>
           </div><br>
           ";
   }
}

function answerEmail($email, $name, $line){
   $webSite        = site_url();
   $to             = $email;
   $name           = $name;
   $headers        = 'TrafikMESTER';
   $subject        = 'Ajánlatkérés';
   $message = "
   Kedves $name!

   Ajánlatkérése megérkezett hozzánk, üzenetét köszönjük! 
   Megtisztelő számunkra, hogy ajánlatkérése során a mi cégünket is számításba vette.
   
   Ajánlatkésérés az alábbi adatokkal regisztráltuk:
   - $line
   
   Ajánlatkérésével kapcsolatban hamarosan felkeresi Önt kollégánk a megadott elérhetőségei egyikén.
   
   Köszönettel:
   TrafikMESTER csapata
   tandofer@tandofer.hu
   +36/76 504-911
   $webSite 
   ";
   $succes = wp_mail($to, $subject, $message, $headers);
   if($succes){
      // echo "sdggfsagasg";
   }
}

function basketListFunc($session_basket_id){
    global $wpdb;
    $table = "session_basket_list";
    $results = $wpdb->get_results(
        "SELECT * FROM $table WHERE session_basket_id = $session_basket_id"
    );

    foreach($results as $value){
        $page_id = $value->p_product;
        $table = "products";
        $row = $wpdb->get_row(
            "SELECT * FROM $table WHERE p_page_id = $page_id"
        );
        $p_name = $row->p_name;
        $line .= $p_name.', ';        
    }
    return $line;
}

function basketIdFunc($session_id){
    global $wpdb;
    $table = "session_basket";
    $row = $wpdb->get_row(
        "SELECT * FROM $table WHERE session_basket_id = '$session_id'"
    );
    $session_basket_id = $row->id;
    return $session_basket_id;
}

