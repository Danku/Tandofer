<?php
/*
Plugin Name: __MY REGISTER
Plugin URI: www.danku.nhely.hu
Description: [my_register_form] 
Version: 1.0
Author: Danku Attila Zsolt
Author URI: 
License: MIT License 
*/
//[my_register_form]

wp_enqueue_style (
    'my-style',
    plugins_url( 'my-plugins/css/my.css' ),
    array(),
    date('YmdHis')
);

function registration_shortcode(){
    ob_start();
    include 'inc/my-register.php';
    
    return ob_get_clean();
}

add_shortcode('my_register_form', 'registration_shortcode');
