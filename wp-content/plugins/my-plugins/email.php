<?php
/*
Plugin Name: __MY EMAIL
Plugin URI: www.danku.nhely.hu
Description: [my_email_form] 
Version: 1.0
Author: Danku Attila Zsolt
Author URI: 
License: MIT License 
*/
//[my_email_form]

wp_enqueue_style (
    'my-style',
    plugins_url( 'my-plugins/css/my.css' ),
    array(),
    date('YmdHis')
);

function add_my_email_scripts() {
   // Az ajax handler script beszúrása az oldal láblécébe.
	wp_enqueue_script (
		'my_angular',
		plugins_url( 'my-plugins/js/angular.min.js' ),
		array('jquery'),
		date('YmdHis')
    ); 
 }
 add_action( 'wp_enqueue_scripts', 'add_my_email_scripts' );

function email_shortcode(){
     ob_start();
     
     include 'inc/my-email.php';
    
     return ob_get_clean();
 }
 add_shortcode('my_email_form', 'email_shortcode');