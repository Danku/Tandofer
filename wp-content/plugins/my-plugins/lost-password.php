<?php
/*
Plugin Name: __MY LOST PASSWORD 
Plugin URI: www.danku.nhely.hu
Description: [my_lost_password_send]/[my_lost_password_get]  
Version: 1.0
Author: Danku Attila Zsolt
Author URI: 
License: MIT License 
*/
//[my_lost_password_send]
//[my_lost_password_get] 

wp_enqueue_style (
    'my-style',
    plugins_url( 'my-plugins/css/my.css' ),
    array(),
    date('YmdHis')
);

//Kérés új jelszó küldése
function lost_passwd_shortcode_send(){
    ob_start();

    if( ! is_user_logged_in()){
        include 'inc/my-lost-passwd-send.php';
    } 
    return ob_get_clean();
}
add_shortcode('my_lost_password_send', 'lost_passwd_shortcode_send');


//jelszó megváltoztatása
function lost_passwd_shortcode_get(){
    ob_start();

    if( ! is_user_logged_in()){
        include 'inc/my-lost-passwd-get.php';
    } 
    return ob_get_clean();
}
add_shortcode('my_lost_password_get', 'lost_passwd_shortcode_get');