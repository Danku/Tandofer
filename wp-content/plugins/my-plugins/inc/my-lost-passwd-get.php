<?php
global $wpdb;
$password1Err=$password2Err=$password1=$password2=$password="";
$hidden=0;
//url get data
$urlTmb = $_GET;
$randomString=$urlId="";
foreach ($urlTmb as $value) {
    if($randomString == ""){
        $randomString = $value;
    } elseif($urlId == ""){
        $urlId = $value;
    }
}

//echo "kod: $randomString<br>username: $userName";
//user ID lekérése
/*$result = get_user_by('login', $userName);
$user_id = $result->ID;*/
//echo $user_id;
//megkeressük az azonosító url-t
$table = 'lost_password';
$result = $wpdb->get_row(
    "SELECT * FROM $table WHERE id=$urlId"
);
$userUrl = $result->user_url;
$user_id = $result->user_id;

//ha megegyezik az url azonosító csak akkor fut le és tölti be az oldalt
if( ! strcmp($userUrl, $randomString) && $hidden==0 && !empty($randomString)){
//update password
if(isset($_POST['new-password'])){
    $hidden = 1; 
    if(isset($_POST['password1']) && isset($_POST['password2'])){
        $password1       = $_POST['password1']; //esc_attr($_POST['password1']);
        $password2       = esc_attr($_POST['password2']);
        //A jelszavak megfelelő erősségűek-e?
        if( ! preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,16}/", $password1)){
            $password1Err = '  Hibás jelszó!';
        }
        //A jelszavak egyeznek-e?
        if( ! strcmp($password1, $password2)){
            $password = $password1;
        } else {
            $password2Err = 'A jelszavak nem egyeznek meg!';
        }  

        if($password1Err=="" && $password2Err=="" && $password != ""){
            $userData = [
                'ID' => $user_id,
                'user_pass' => $password
            ];
            $succes = wp_update_user( $userData );
            if(is_wp_error($succes)){
               echo "Nem sikerült";
            } else {
                $where = [ 'id' => $urlId ];
                $deleteOk = $wpdb->delete($table, $where);
                if($deleteOk == true){ succesfunc(); }              
            }
        }                
    }
}
if($hidden == 0 || $password1Err!="" || $password2Err!=""){
?>
<div id="reset_form">
    <form method="post" class="form">
        <h2>Új jelszó létrehozása</h2>
        <label for="password1">Jelszó:  </label><?php if($password1Err!="")
            {echo "<span class='error'>* $password1Err </span> <br> 
                A jelszó minimum 8 karakter hosszúságú.
                 Használjon kis és NAGY betűt, illetve számot is!";} 
                 else
                  {echo " * A jelszó minimum 8 karakter hosszúságú.
                 Használjon kis és NAGY betűt, illetve számot is!";}?>
            <br>
            <input type="password" name="password1" pattern="[a-zA-Z0-9áéíóöőüúűÁÉÍÓÖŐÜÚŰ]+" 
                value="" required class="input" placeholder="********"
                id="password1"/>
                <br>    
        
        <label for="password2">Jelszó megerősítése:  </label> <?php if($password2Err!="")
            {echo "<span class='error'>* $password2Err </span>";} else {echo " *";}?>
            <br>
            <input type="password" name="password2" pattern="[a-zA-Z0-9áéíóöőüúűÁÉÍÓÖŐÜÚŰ]+" 
                value="" required class="input" placeholder="********"
                id="password2"/><br> 
                <br>
        <input type="submit" class="mobileButton" name="new-password" value="Új jelszó">
    </form>
</div>

<?php 
    } 
}

function succesFunc(){
    echo "
        <h3>Sikeresen megváltoztattad a jelszavad!</h3>
        <a href='index.php?p=13'>Jelentkezz be</a>
    ";
}

?>