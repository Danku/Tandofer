<?php 
echo "<h2 class='input form'>Bejelentkezés</h2>";

get_header();
global $wpdb;

//LOGIN FUNC
if(isset($_POST['login_Submit'])) {
	if(isset($_POST['userName']) && isset($_POST['password'])){
		$userName			= sanitize_text_field( trim($_POST['userName']) );
		$password			= esc_attr(trim($_POST['password']));
		$remember			= isset( $_POST['rememberMe'] ) ? sanitize_text_field( $_POST['rememberMe'] ) : '';
		$redirect_to        = esc_url_raw( $_POST['redirect_to'] );
		$secure_cookie      = null;
		
			$creds = [
				'user_login' => $userName,
				'user_password' => $password,
				'remember' => $remember
			];
			//Beléptetést végzi el
			$user = wp_signon($creds, $secure_cookie);
			$userID = $user->ID;
			$userPass = $user->user_password;

			wp_set_current_user($userID, $userName);
			wp_set_auth_cookie($userID, true, false);
			do_action('wp_login', $userName);

			if(is_user_logged_in()){
				//át irányít a megadott oldalra
				wp_safe_redirect( $redirect_to );	
			} else {
				echo "<span id='3' class='form' style='color:red; font-size:20px;'>
				Hibás felhasználónév vagy jelszó!
				</span>";
			}
		}
	}

//ha bejelnetkzett a felahsználó elrejtjük a belépő formot
?>

<!--LOGIN FORM-->
<form method="post" id="login-form" class="form">
	        
	<label for="user_login">Felhasználónév: </label><span> * </span><br>
	<input type="text" name="userName" id="user_login" 
		placeholder="Felhasználónév vagy email cím" class="input" value="" required >
		<br>

	<label for="user_pass">Jelszó: </label><span> *</span><br>
	<input type="password" name="password" id="user_pass" 
	placeholder="********" class="input" value="" required >
	<br>        
        
	<input name="rememberMe" type="checkbox" id="rememberme" value="forever"> 
	<label for="rememberme" class="check" > Bejelentkezve maradok</label>

    <span class="lost_pass_mobile lost_pass_PC"><a href="<?php echo site_url();?>/lost-password/?lost-pass-request">Elfelejtett jelszó</a></span><br><br>
	<input type="submit" name="login_Submit" class="mobileButton" value="Bejelentkezés">
	<input type="hidden" name="redirect_to" value="<?php bloginfo('url'); ?>/bejelentkezes-regisztracio/">
</form>


