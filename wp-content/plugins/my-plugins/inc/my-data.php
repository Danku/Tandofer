<?php
  global $current_user, $wpdb;
  get_currentuserinfo();
  $user_id = $current_user->ID;
  $displayName = $current_user->display_name;
  $email = $current_user->user_email;
  //jelszó
  //phoneNumber
  $table = 'users_phone';
  $phone = $wpdb->get_row(
          "SELECT * FROM $table WHERE user_id = $user_id"
  );
  $phoneNumber = $phone->user_phone;

  $displayNameErr=$emailErr=$phoneNumberErr=$passwd1Err=$passwd2Err=$password="";

  //UPDATE USER DATA
  if(isset($_POST['update_user'])){
      //teljes név
      if(isset($_POST['displayName'])){
        $displayName       = sanitize_text_field($_POST['displayName']);
        if(6 > strlen($displayName)){
            $displayNameErr='  Túl rövid a teljes neved, minimum 6 karakter.';
        }
        if( ! preg_match("/^[a-zA-ZáéíóöőüűúÁÉÍÓÖŐÜŰÚ ]*$/", $displayName)){
            $displayNameErr = "Csak betűt és szóközt használjon.";
        }
        //A teljes név max 40 karakter
        if( 30 < $displayName){
            $displayNameErr = '  A túl hosszó a Teljes név';
        }
        if($displayNameErr==""){
            $displayNameOk=$displayName;
        }                
    }
    //email cím
    if(isset($_POST['email'])){
        $emailget          = sanitize_email($_POST['email']);

        //az email cím érvényes-e?
        if( ! is_email($email)){
            $emailErr = '  Az email cím érvénytelen!';
        }

        //két email megegyezik-e
        if( ! strcmp($emailget,$email)){
            $emailOk = $email;
        } else {
            if( email_exists($emailget)){
                $emailErr = " Ez az email cím már foglalt! ( $emailget )";
                $email = "";
            }
        }
        
        if($emailErr==""){
            $emailOk=$email;
        }
    }
    //telefonszám ellenőrzés
    if(isset($_POST['phoneNumber'])){
        $phoneNumber    = sanitize_text_field($_POST['phoneNumber']);
        if( ! preg_match("/^[0-9 +\/]*$/", $phoneNumber)){
            $phoneNumberErr = 'A telefonszám csak számokat tartalmazhat';
        }
        if(strlen($phoneNumber)<15){
            $phoneNumberErr = "Hibás telefonszám!";
        }
        if($phoneNumberErr==""){
            $phoneNumberOk=$phoneNumber;
        }
    }
    //Jelszó
    if(isset($_POST['passwd1']) && isset($_POST['passwd2'])){
        $passwd1       = esc_attr($_POST['passwd1']);
        $passwd2       = esc_attr($_POST['passwd2']);
        //A jelszavak megfelelő erősségűek-e?
        if( ! preg_match("/^(?=.*[A-ZÁÉÍÓÖŐÜŰÚ])(?=.*[a-záéíóöőüűú])(?=.*[0-9]).{8,16}/", $passwd1)){
            $passwd1Err = 'Hibás jelszó!';
        }
        //A jelszavak egyeznek-e?
        if( ! strcmp($passwd1, $passwd2)){
            $password = $passwd1;
        } else {
            $passwd2Err = 'A jelszavak nem egyeznek meg';
        }  
        if($passwd1Err=="" && $passwd2Err==""){
            $password=$passwd1;
        }
    }
    //FRISSÍTÉS
    $succesData = 0;
    if($displayNameErr =="" && $emailErr =="" && $phoneNumberErr ==""
        && $passwd1Err =="" && $passwd2Err =="" ){
            if($displayNameOk !="" && $password !="" && $phoneNumberOk !=""
                && $emailOk !=""){
                    $userdata = [
                        'ID' => $user_id,
                        'user_pass' => $password,
                        'user_email' => $emailOk,
                        'display_name' => $displayNameOk
                    ];

                    $succes1 = wp_update_user(  
                        $userdata            
                    );
                    if(is_wp_error($succes1)){
                        //echo "Nem sikerült frissíteni";
                        $succesData = 0;
                    } else {
                        //echo "siker";
                        $succesData = 1;
                    }

                        global $wpdb;
                        $table = 'users_phone';
                        $data = [
                            'user_phone' => $phoneNumberOk
                        ];
                         $where = [
                            'user_id' => $user_id
                         ];

                         $succes2 = $wpdb->update($table, $data, $where);
                                           
                }
        }
  }  

  //html
?>
<div>
  <h3>Üdvözlöm <?php echo $displayName;?>! </h3>
  Köszönjük, hogy bejelentkezett.<br>
  Ha szükséges az adatait itt tudja szerkeszteni.<br><br>            
  
  <b onclick="myFunc()">Adataim:  </b><a href="#"><i class="fa fa-edit" onclick="myFunc()" style="color: #72162f;">szerkesztés </i></a><br>
  <?php
  echo "
  $displayName<br>
  $email<br>
  $phoneNumber<br>
  ";
  ?>
  <br>
    <a href="<?php echo wp_logout_url().'/bejelentkezes-regisztracio'; ?>" class="log-out-button mobileButton">KIJELENTKEZÉS</a><br>
  <?php 
  if($succesData == 1){
      echo "
            <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
				<div class='elementor-widget-container'>
                <div class='elementor-alert elementor-alert-danger' role='alert'>
                <span class='elementor-alert-title'>
                    Az adataid sikeresen frissültek!
                </span>
                <span class='elementor-alert-description'>
                </span>
                <button type='button' class='elementor-alert-dismiss'>
                <span aria-hidden='true'>&times;</span>
                <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
				</button>
                </div>
				</div>
			</div><br>
            ";
  }
  ?>
  

<?php
if(empty($displayNameErr) && empty($emailErr) && empty($passwd1Err) &&
         empty($passwd2Err) && empty($phoneNumberErr) ){
    //echo "<button onclick='myFunc()' class='mobileButton' id='button'>Szerkesztés</button>";
    echo "<form method='post' class='data-form' id='form'>";
} else 
{
    echo "<form method='post'>";   
}
?><!--Teljesnév -->
  <label for="displayName">Teljes név:</label><?php if($displayNameErr!="")
            {echo "<span class='error'> *  $displayNameErr</span>";} else {echo " *";}?>
            <br>
  <input type="text" value="<?php echo $displayName;?>" id="displayName" name="displayName"
          class="input" pattern="[a-zA-ZáéíóöőüűúÁÉÍÓÖŐÜŰÚ ]+"  required/><br>

<!--email cím -->
  <label for="email">Email cím:</label><?php if($emailErr!="")
            {echo "<span class='error'> *  $emailErr</span>";} else {echo " *";}?>
            <br>
  <input type="email" value="<?php echo $email;?>" id="email" name="email"
          class="input" required/><br>

<!--telefonszám -->
  <label for="phoneNumber">Telefonszám:</label><?php if($phoneNumberErr!="")
            {echo "<span class='error'> *  $phoneNumberErr</span>";} else {echo " *";}?>
            <br>
  <input type="text" class="phone input" value="<?php echo $phoneNumber;?>" id="phoneNumber" name="phoneNumber"
             pattern="[0-9/+ ]+"  required/><br>

<!--jelszó -->
  <label for="passwd1">Jelszó:</label><?php if($passwd1Err!="")
            {echo "<span class='error'> * $passwd1Err </span> <br> 
                A jelszó minimum 8 karakter hosszúságú.
                 Használjon kis és NAGY betűt, illetve számot is!";} 
                 else
                  {echo " * A jelszó minimum 8 karakter hosszúságú.
                 Használjon kis és NAGY betűt, illetve számot is!";}?>
                 <br>
  <input type="password" name="passwd1" id="passwd1" 
            class="input" placeholder="********" 
            pattern="[a-zA-Z0-9áéíóöőüúűÁÉÍÓÖŐÜÚŰ]+" required/><br>

<!--jelszó megerősítés -->
  <label for="passwd2">Jelszó megerősítése:</label><?php if($passwd2Err!="")
            {echo "<span class='error'> * $passwd2Err </span>";} else {echo " *";}?>
            <br>
  <input type="password" name="passwd2" id="passwd2" 
            class="input" placeholder="********" 
            pattern="[a-zA-Z0-9áéíóöőüúűÁÉÍÓÖŐÜÚŰ]+" required/><br>
  <br>

<!--küldés -->
  <input type="submit" class="mobileButton" name="update_user" class="input" value="Adatok frissítése">
</form>
</div>

<!--Script funkciók -->
<script>
    function myFunc(){
        document.getElementById("form").style.display = "inline-block";
        document.getElementById("button").style.display = "none";
    }
    setTimeout(function() {
    document.getElementById("1").style.display = "none";
    }, 5000);
</script>