

<?php

//[my_register-form]
//űrlap létrehozása
$hidden=0;

if(isset($_POST['reg_submit'])){
$username=$password1=$password2=$email=$phoneNumber=$fullName=$password="";
$usernameErr=$password1Err=$password2Err=$emailErr=$phoneNumberErr=$fullNameErr="";

   
    //$website        = esc_url($_POST['website']); //esc_textarea
    
    if(isset($_POST['username'])){
        $username       = sanitize_user($_POST['username']);
        if( ! preg_match("/^[a-zA-Z0-9]*$/", $username)){
            $usernameErr = "Csak betűket és számokat használjon!";
        }
        //Megfelelő hosszúságú e a felhasználó név
        if(5>strlen($username)){
            $usernameErr = "A felhasználónév minimum 5 karakterből álljon!";
        }
        //A felhasználónév létezik-e?
        if(username_exists($username)){
            $usernameErr = '  Ez a felhasználónév már foglalt!';
        }
        //validate_username függvény segít h érvényes legyen a felhasználó név
        if( ! validate_username($username)){
            $usernameErr = '  Hibás felhasználónév!';
        }
        if($usernameErr==""){
            $usernameOk=$username;
        }
    }
    if(isset($_POST['fullName'])){
        $fullName       = sanitize_text_field($_POST['fullName']);
        if(6 > strlen($fullName)){
            $fullNameErr='  Kérjük adja meg a teljes nevét!';
        }
        if( ! preg_match("/^[a-zA-Z ]*$/", $fullName)){
            $fullNameErr = "Csak betűket és szóközt használjon!";
        }
        //A teljes név max 40 karakter
        if( 30 < $fullName){
            $fullNameErr = '  A teljes név maximum 30 karakterből állhat!';
        }
        if($fullNameErr==""){
            $fullNameOk=$fullName;
        }                
    }
    if(isset($_POST['email'])){
        $email          = sanitize_email($_POST['email']);
        //Az email cím létezik-e?
        if(email_exists($email)){
            $emailErr = '  Ezzel az email címmel már regisztráltak!';
        }
        //az email cím érvényes-e?
        if( ! is_email($email)){
            $emailErr = '  Az email cím érvénytelen!';
        }
        if($emailErr==""){
            $emailOk=$email;
        }
    }
    if(isset($_POST['password1']) && isset($_POST['password2'])){
        $password1       = esc_attr($_POST['password1']);
        $password2       = esc_attr($_POST['password2']);
        //A jelszavak megfelelő erősségűek-e?
        if( ! preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,16}/", $password1)){
            $password1Err = '  Hibás jelszó!';
        }
        //A jelszavak egyeznek-e?
        if( ! strcmp($password1, $password2)){
            $password = $password1;
        } else {
            $password2Err = 'A jelszavak nem egyeznek meg!';
        }  
        if($password1Err=="" && $password2Err==""){
            $password=$password1;
        }
    }
    //Telefonszám ellenőrzése
    if(isset($_POST['phoneNumber'])){
        $phoneNumber    = sanitize_text_field($_POST['phoneNumber']);
        if( ! preg_match("/^[0-9 +\/]*$/", $phoneNumber)){
            $phoneNumberErr = 'Hibás telefonszám!';
        }
        if(strlen($phoneNumber)<15){
            $phoneNumberErr = "Hibás telefonszám!";
        }
        /*global $wpdb;
        $table='users_phone';
        $result=$wpdb->get_row(
            "SELECT id FROM $table WHERE user_phone = $phoneNumber"
        );
        if( $result->id !=""){
            $phoneNumberErr = "Ezzel a telefonszámmal már regisztráltak";
        }*/
        if($phoneNumberErr==""){
            $phoneNumberOk=$phoneNumber;
        }
    }
    
    //Webhely érvényes-e?
    /*    if( ! empty($)){
        if( ! filter_var($website, FILTER_VALIDATE_URL)){
            echo 'Weboldal url-je nem megfelelő';
        }
    }*/
    $check1     = $_POST['check1'];
    $check2     = $_POST['check2'];
    $check3     = $_POST['check3'];

    if($check1 =="" && $check2 =="" && $check3==""){
        $checkErr="<span style='color : red;'>A regisztrációhoz szükséges az alábbi nyilatkozatok elfogadása:</span><br>";
    }
   

    if($usernameOk!="" && $emailOk!="" && $phoneNumberOk!="" && 
        $fullNameOk!="" && $password!=""){
                if($check1 != "" && $check2 != "" && $check3 != ""){
                global $wpdb;
                $userdata = array(
                    'user_login'  =>  $usernameOk,
                    'user_pass'    =>  $password,
                    'display_name'  => $fullNameOk,
                    'user_email'     => $emailOk                    
                );
                
                $user_id = wp_insert_user( $userdata );                
                //On success
                if ( ! is_wp_error( $user_id ) ) {
                //Telefonszám
                $table='users_phone';
                $data = [
                    'user_id' => $user_id,
                    'user_phone' => $phoneNumberOk
                ];
                $formats = [ '%d', '%s' ];
                $wpdb->insert($table, $data, $formats);

                emailfunc($fullNameOk, $emailOk, $usernameOk, $password, $phoneNumberOk);
                $hidden=1;

                }
            }
        }     
                           
}

    $logged = is_user_logged_in();    
    if($logged == false && $hidden==0){
    ?>
    
    
   <!-- <span onclick="myClick()"> -->
        <h2 class="input form">Regisztráció</h2>
<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post" class="form">
         <span><?php echo $myError;?></span>
        <label for="username">Felhasználónév:  </label><?php if($usernameErr!="")
            {echo "<span class='error'>* $usernameErr</span>";} else {echo " *";}?>
            <br>
            <input type="text" name="username" value="<?php echo $username;?>"
                pattern="[a-zA-Z0-9 ]+" required class="reg" placeholder="Felhasználónév"
                id="username"/>
                <br>     
        
        <label for="fullName">Teljes név:  </label><?php if($fullNameErr!="")
            {echo "<span class='error'>*  $fullNameErr</span>";} else {echo " *";}?>
            <br>
            <input type="text" name="fullName" value="<?php echo $fullName;?>"
                pattern="[a-zA-Z ]+" required class="reg" placeholder="Teljes név"
                id="fullName"/>                    
       <!-- </span>
        <?php
        /* if($usernameErr =="" & $fullNameErr=="" && $phoneNumberErr=="" &
                $emailErr=="" & $password1Err=="" & $password2Err==""){
                    echo "<span id='reg-form-hidden' style='display:none;'>";
                }*/
        ?>-->
            
        <label for="email">Email cím:  </label><?php if($emailErr!="")
            {echo "<span class='error'>*  $emailErr</span>";} else {echo " *";}?>
            <br>
            <input type="email" name="email" value="<?php echo $email; ?>"
                required class="reg" placeholder="@Example.com"
                id="email"/>
                <br>    
        
        <label for="phoneNumber">Telefonszám:  </label><?php if($phoneNumberErr!="")
            {echo "<span class='error'>*  $phoneNumberErr</span>";} else {echo " * Csak számot írjon!";}?>
            <br>
            <input type="tel" name="phoneNumber" class="phone reg" value="<?php echo $phoneNumber;?>"
                pattern="[0-9/+ ]+" required placeholder="+36/xx xxx xx xx"
                id="phoneNumber"/>
                <br>    
        
    <!--  <label for="website">Web oldal: </label><br>
            <input type="text" name="website" 
                /><br>
    -->    
        <label for="password1">Jelszó:  </label><?php if($password1Err!="")
            {echo "<span class='error'>* $password1Err </span> <br> 
                A jelszó minimum 8 karakter hosszúságú.
                 Használjon kis és NAGY betűt, illetve számot is!";} 
                 else
                  {echo " * A jelszó minimum 8 karakter hosszúságú.
                 Használjon kis és NAGY betűt, illetve számot is!";}?>
            <br>
            <input type="password" name="password1" 
                pattern="[a-zA-Z0-9áéíóöőüúűÁÉÍÓÖŐÜÚŰ]+" required class="reg" placeholder="********"
                id="password1"/>
                <br>    
        
        <label for="password2">Jelszó megerősítése:  </label> <?php if($password2Err!="")
            {echo "<span class='error'>* $password2Err </span>";} else {echo " *";}?>
            <br>
            <input type="password" name="password2" 
                pattern="[a-zA-Z0-9áéíóöőüúűÁÉÍÓÖŐÜÚŰ]+" required class="reg" placeholder="********"
                id="password2"/><br> 
                <br>                
                    
            <?php
            if($checkErr!=""){
                echo $checkErr;
            } else {
                echo "A regisztrációhoz szükséges az alábbi nyilatkozatok elfogadása:<br>";
            }
            ?>

        <input type="checkbox" name="check1" value="adatkez" id="check1" required>  <label for="check1" class="check">Megértettem és tudomásul </label>veszem az
        <a href="index.php?p=1031" target="blank">  Adatkezelési Tájékoztatót.</a>
        <br>
        <input type="checkbox" name="check2" value="altalanos" id="check2" required> <label for="check2" class="check">Elfogadom az</label>
         <a href="index.php?p=1035" target="blank">  Általános Szerződési Feltételeket.</a>
        <br>
        <input type="checkbox" name="check3" value="GDPR" id="check3" required>
         <label for="check3" class="check">Hozzájárulok, hogy </label> kapcsolattartás céljából a szolgáltató az általam megadott adatokat felhasználhatja. 
        
        <br>       
                    
        <!-- </span>
        <?php
            /*if($usernameErr =="" & $fullNameErr=="" && $phoneNumberErr=="" &
            $emailErr=="" & $password1Err=="" & $password2Err==""){
                echo "
                <p style='font-size:19px; margin-left:20px;' id='pont-pont'>
                <strong>. . .</strong>
                </p> 
                ";
            } */
        ?>-->
            <br>    
            <input type="submit" class="mobileButton" name="reg_submit" value="Regisztráció"/>
</form>

   <!-- <script>
        function myClick(){
            document.getElementById("reg-form-hidden").style.display = "inline-block";
            document.getElementById("pont-pont").style.display = "none";
        }
    </script> -->

    <?php
    }

    function emailfunc($fullNameOk, $emailOk, $usernameOk, $password, $phoneNumberOk){
        $to = $emailOk;
        $webSite = site_url();
        $login = site_url().'/bejelentkezes-regisztracio/';
        $subject = "Regisztráció";
        $headers = "TrafikMester";
        $message = "
        Kedves $fullNameOk!

        Köszönjük, hogy regisztrált TrafikMESTER honlapján!

        Felhasználói fiókja sikeresen elkészült és most már be tud lépni email címével a regisztrációkor megadott jelszóval az alábbi linken: 
        $login

        Felhasználói fiókjának adatai:
         Felhasználónév: $usernameOk
         Email cím: $emailOk
         Teljes név: $fullNameOk
         Telefonszám: $phoneNumberOk

        Az adatait a későbbiekben honlapunkon tudja módosítani.

        Köszönettel:
        TrafikMESTER csapata
        $webSite 
        ";
        $succes = wp_mail($to, $subject, $message, $headers);
        if($succes){
            echo "
            <h3>Sikeres Regisztráció.</h3>
            Jelentkezz be!
        ";
        }
    }