<?php
global $wpdb;
//Random String generate
function generateRandomString( $length = 27){
    $characters = '0123456789qwertzuiopasdfghjklyxcvbnmASDFGHJKLQWERTZUIOPYXCVBNM';
    $charactersLength = strlen($characters);
    $randomString = '';
    for($i=0; $i<$length; $i++){
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
$randomUrl = generateRandomString();
$keyRandom = generateRandomString(7);
$idRandom = generateRandomString(7);
$rewRandom = generateRandomString(18);
$succes=$error=$error1=$emailOk='';
//new password function
if(isset($_POST['reset-password'])){
    $email = sanitize_email( $_POST['lost-password'] );
    if( ! is_email($email)){
        $error = 'Érvénytelen email cím!';
    }
    if( ! email_exists($email)){
        $error = 'Nincs regisztrálva ilyen email cím!';
    }
    if($error == ''){
        $emailOk = $email;
        $user_id = email_exists($emailOk);
        $user_info = get_userdata($user_id);
        $userName = $user_info->user_login;
        $displayName = $user_info->display_name;
        $webSite = site_url();
        //  $url = "$webSite/lost-password/?key=$randomUrl&login=$userName";
        
        //echo "$user_id<br>$randomUrl";
        //Adatbázisba mentés
        $table = 'lost_password';
        $result = $wpdb->get_row(
            "SELECT * FROM $table WHERE user_id = $user_id"
        );
        $id = $result->id;
        if(empty($id)){
            $data = [
                'user_id' => $user_id,
                'user_url' => $randomUrl
            ];
            $formats = [ '%d', '%s' ];
            $insert = $wpdb->insert($table, $data, $formats);
            $id = $wpdb->insert_id;
        } else {
            $data = [
                'user_id' => $user_id,
                'user_url' => $randomUrl
            ];
            $where = [ 'id' => $id ];
            $update = $wpdb->update($table, $data, $where);
        }
        $url = "$webSite/lost-password/?$keyRandom=$randomUrl&$idRandom=$id&x=$rewRandom";
        //Email kiküldése
        if($insert == true || $update == true){
            $to = $emailOk;
            $subject = "Új jelszó kérése";
            $header = "TrafikMester";
            $name = "TrafikMester csapata";
            $message = "
            Kedves $displayName!

            Ön arról értesített minket, hogy elfelejtette a(z) $userName felhasználó névhez tartozó jelszavát.

            Az új jelszavának beállításához keresse fel a következő címet. 
            $url             
            Amennyiben a fenti linkre kattintás nem működik, a linket másolja ki a böngészőjébe. 
            Ha a jelszócserét nem Ön kezdeményezte, akkor hagyja levelünket figyelmen kívül. 

            Köszönettel: 
            $name
            $webSite
            ";
            $ok = wp_mail($to, $subject, $message, $header);
            if($ok){
                $succes = "Látogass el az email címedre az új jelszó létrehozásáért.";
            }

        }
    }
}

if(isset($_GET['lost-pass-request'])){
?>
<div>
    <form method="post" class="form">
        <?php if( ! empty($error1)) echo "<span class='error'>$error1</span>"; ?>
        <?php echo "$succes<br>"; ?>
        <h2>Elfelejtett jelszó</h2>
        <label for="lost-password">Email cím:</label>
        <?php if( ! empty($error)) echo "<span class='error'>*  $error</span>"; ?><br>
        <input type="email" id="lost-password" name="lost-password" 
            class="input" placeholder="@Example.com" required><br>
        <br>
        <input type="submit" class="mobileButton" name="reset-password" value="Új jelszó">
    </form>
</div>

<?php
}