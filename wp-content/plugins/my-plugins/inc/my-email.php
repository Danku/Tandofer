
<?php   
if(isset($_POST['send-submitted'])){
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    //Secret key
    $privatekey = "6Lfhb2YUAAAAAMaySQxz_7csBUYmLj1kgCOwDi1i";

    $response = file_get_contents($url."?secret=".$privatekey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $data = json_decode($response);

    if(isset($data->success) AND $data->success==true){
        //true
        sendMail();
    } else {
        echo "
            <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
                <div class='elementor-widget-container'>
                <div class='elementor-alert elementor-alert-danger' role='alert'>
                <span class='elementor-alert-title'>
                    Kérjük, jelölje meg a \"Nem vagyok robot\" ablakot!
                </span>
                <span class='elementor-alert-description'>
                    reCAPTCHA hiba lépett fel. Kérjük ismételje meg újra.
                </span>
                <button type='button' class='elementor-alert-dismiss'>
                <span aria-hidden='true'>&times;</span>
                <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
                </button>
                </div>
                </div>
            </div><br>
            ";
    }
}

?>

<!--HTML EMAIL FORM-->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<form action="<?php echo esc_url($_SERVER['REQUEST_URI']);?>" method="post" ng-app="" >

<label for='send-name'>Név: </label><span class="csillag">*</span><br>
<input type="text" name="send-name"  size="30" pattern="^[a-zA-ZáéíóöőüűúÁÉÍÓÖŐÜŰÚ ]*$" autofocus
    required ng-model="sendName" class="mail" placeholder="Teljes név"><br>

<label for='send-phone'>Telefonszám: </label><span class="csillag">*</span><br>
<input type="text" class="phone mail" name="send-phone" size="30" pattern="^[0-9 +/]*$"
    required ng-model="sendPhone" placeholder="+36/xx xxx xx xx" 
    /><br>

<label for='send-mail'>Email cím: </label><span class="csillag">*</span> <br>
<input type="email" name="send-mail" size="30" 
    required data-ng-model="sendEmail"
    class="mail" placeholder="@Example.com"/><br>

<label for='send-message'>Üzenet: </label><span class="csillag">*</span> <br>
<textarea rows="10" cols="10" name="send-message" id="message_id" maxlength="1000">
Tisztelt Hölgyem / Uram!







Üdvözlettel:
{{sendName}}
{{sendPhone}}
{{sendEmail}}
</textarea><br>

<div class="g-recaptcha" data-sitekey="6Lfhb2YUAAAAAEojPCav9Rr6A5TZxbmhlYqBlSbW"></div><!-- site key -->
<span>
<input type="checkbox" name="check" id="check" value="1" required >
<label for="check" class="check">Hozzájárulok, hogy </label> kapcsolattartás céljából a szolgáltató az általam megadott adatokat felhasználhatja. 
<a href="index.php?p=1031" target="_blank">Adatkezelési tájékoztató.</a>
</span>
<br><br>
<input type="submit" class="mobileButton" name="send-submitted" value="Küldés"/>
</form>

<?php

function sendMail(){
    //átadjuk az adatokat és megtisztítjuk
        //email vizsgálata
        //esc_textarea összeszedi az üzenet karaktereit
        $name       = sanitize_text_field($_POST["send-name"]);
        $email      = sanitize_email($_POST["send-mail"]);
        $phone      = sanitize_text_field($_POST["send-phone"]);
       // $subject    = sanitize_text_field($_POST["send-subject"]);
        $subject    = "Kapcsolat felvétel";
        $message    = esc_textarea($_POST["send-message"]);
        $check      = $_POST['check'];
        $webSite    = site_url();
        // lekérjük az adminisztrátor email címét
        $to = get_option('admin_email');
    
        $message=$message;
        $headers = "Form: $name <$email>"."\r\n";

        if($check ==1){
        // Feldolgozza az email küldést
        //echo "elküldve";
        $succes=wp_mail($to, $subject, $message, $headers);
        if($succes){
            answerEmail($email, $name, $webSite);
            echo "
            <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
				<div class='elementor-widget-container'>
                <div class='elementor-alert elementor-alert-danger' role='alert'>
                <span class='elementor-alert-title'>
                    Köszönjük, hogy felvette velünk a kapcsolatot.
                </span>
                <span class='elementor-alert-description'>
                    Email elküldve.
                </span>
                <button type='button' class='elementor-alert-dismiss'>
                <span aria-hidden='true'>&times;</span>
                <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
				</button>
                </div>
				</div>
			</div><br>
            ";
        } else {
            echo "
            <div data-id='9dd922a' class='elementor-element elementor-element-9dd922a elementor-widget elementor-widget-alert' data-element_type='alert.default'>
				<div class='elementor-widget-container'>
                <div class='elementor-alert elementor-alert-danger' role='alert'>
                <span class='elementor-alert-title'>
                    Váratlan hiba lépett fel. Kérjük ismételje meg újra.
                </span>
                <span class='elementor-alert-description'>
                   
                </span>
                <button type='button' class='elementor-alert-dismiss'>
                <span aria-hidden='true'>&times;</span>
                <span class='elementor-screen-only'>Figyelmeztetés elutasítása</span>
				</button>
                </div>
				</div>
			</div><br>
            ";
        }
    }
}

function answerEmail($email, $name, $webSite){
    //echo $email.$name;
    $to             = $email;
    $name           = $name;
    $headers        = 'TrafikMESTER';
    $subject        = 'Kapcsolat felvétel';
    $message = "
        Kedves $name!

        Köszönjük, hogy felvette velünk a kapcsolatot!

        Kollégánk hamarosan felkeresi a megadott elérhetőségek egyikén.  

        Köszönettel:
        TrafikMESTER csapata
        tandofer@tandofer.hu
        +36/76 504-911
        $webSite
    ";
    $succes = wp_mail($to, $subject, $message, $headers);
    if($succes){
       // echo "sdggfsagasg";
    }
}