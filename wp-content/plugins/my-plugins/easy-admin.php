<?php
/*
Plugin Name: adminisztráció
Plugin URI: www.danku.nhely.hu
Description: adminisztráció 
Version: 1.0
Author: Danku Attila Zsolt
Author URI: 
License: MIT License 
*/
// bejelentkezési oldal logo
function custom_login_logo(){
    echo '<style> h1 a, h1 a:hover, h1 a:focus {
        font-size : 1.4em;
        font-weight : normal;
        text-align : center;
        text-indent : 0;
        line-height : 1.1em;
        text-decoration : none;
        color : #dadada;
        text-shadow : 0 -1px 1px #666, 0 1px 1px #fff;
        background-image : none !important;
    }</style>';
}
add_action('login_head', 'custom_login_logo');

// adminisztrácios oldal fejléc logo levétele
function remove_admin_logo(){
    echo '<style> img#header-logo{
        display : none; 
    }</style>';
}
add_action('admin_head', 'remove_admin_logo');

//wordpress lábléc szöveg módosítása
function change_footer_admin(){
    echo date('Y').' - lábléc szöveg 
    <a href="mailto:dankuatesz@gmail.com">mailto:dankuatesz@gmail.com</a>';
}
add_filter('admin_footer_text', 'change_footer_admin');

//admin menü sáv eltávolítása
add_filter('show_admin_bar', '__return_false');


?>