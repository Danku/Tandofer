<?php
/*
Plugin Name: __MY LOGIN 
Plugin URI: www.danku.nhely.hu
Description: [my_login_form] 
Version: 1.0
Author: Danku Attila Zsolt
Author URI: 
License: MIT License 
*/
//[my_login_form]

wp_enqueue_style (
    'my-style',
    plugins_url( 'my-plugins/css/my.css' ),
    array(),
    date('YmdHis')
);

function login_shortcode(){
    ob_start();

    if(is_user_logged_in()){
        include 'inc/my-data.php';
    } else {
        include 'inc/my-login.php';
    }

    return ob_get_clean();
}

add_shortcode('my_login_form', 'login_shortcode');