<?php
global $wpdb;
$table = 'products';
$products = $wpdb->get_results(
    "SELECT * FROM $table"
);

if(isset($_GET['product'])){
    $p_page_id = $_GET['product'];
    
    if($p_page_id != 'error'){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: index.php?p=$p_page_id");
    }
}

?>
<div>
    <form method="get">
        <select name="product" class="select">
            <option value="error">Válasszon terméket!</option>
            <option value="1556" style="font-weight: 600;"><b>I. SZOFTVEREK</b></option>
            <?php
            foreach ($products as $key => $value) {
                $p_page_id = $value->p_page_id;
                $p_name = $value->p_name;
                $p_category = $value->p_category;
                if($p_category == 1){
                echo "
                    <option value='$p_page_id'> - $p_name</option>            
                ";}
                } 
            ?>
            <option value="2091" style="font-weight: 600;"><b>II. HARDVEREK</b></option>
            <?php
            foreach ($products as $key => $value) {
                $p_page_id = $value->p_page_id;
                $p_name = $value->p_name;
                $p_category = $value->p_category;
                if($p_category == 2){
                echo "
                    <option value='$p_page_id'> - $p_name</option>            
                ";}
                } 
            ?>
            <option value="1278" style="font-weight: 600;"><b>III. SZOLGÁLTATÁSOK</b></option>
            <?php
            foreach ($products as $key => $value) {
                $p_page_id = $value->p_page_id;
                $p_name = $value->p_name;
                $p_category = $value->p_category;
                if($p_category == 3){
                echo "
                    <option value='$p_page_id'> - $p_name</option>            
                ";}
                } 
            ?>
        </select>
        <input type="submit" class="mobileButton" value="Keresés">        
    </form>
    <!--<a href="javascript:history.go(-1)">link text here...</a>-->
</div>