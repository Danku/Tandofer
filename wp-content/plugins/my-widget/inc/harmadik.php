<div class="right szoveg centermobile ">
    <b class="cim">NYILATKOZATOK</b><br>
    <a href="index.php?p=1031">Adatkezelési nyilatkozat</a> <br>
    <a href="index.php?p=1035">Általános szerződési feltételek</a> <br>
<br>
<span class="hiddenPC">
    <br>
    <a href="https://www.tandofer.hu/" target="_blank">
        <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/TDF_White_2500x1000-1024x410.png" 
            class="image wp-image-941  attachment-medium size-medium" alt="">
    </a>
    <br><br>
</span>
<span class="hiddenPC">
    <a href="tel:3676504911">
        <img class="size-medium wp-image-1961 contact" src="<?php echo site_url();?>/wp-content/uploads/2018/08/phone_contact-e1533194691458-286x300.png" alt="" width="60" />
    </a>
    <a href="index.php?p=11">
        <img class="size-medium wp-image-1935 contact" src="<?php echo site_url();?>/wp-content/uploads/2018/08/letter_contact-286x300.png" alt="" width="60" />
    </a>
    <a href="https://www.google.com/maps/place/Kecskem%C3%A9t,+Munk%C3%A1csy+u.+47,+6000+Magyarorsz%C3%A1g/@46.908924,19.68519,18z/data=!4m5!3m4!1s0x4743da0e2f9dc1a1:0x6a3eef7ef394abbe!8m2!3d46.908924!4d19.6851901?hl=hu-HU" target="_blank" rel="noopener">
        <img class="size-medium wp-image-1962 contact" src="<?php echo site_url();?>/wp-content/uploads/2018/08/address_contact-286x300.png" alt="" width="60" />
    </a>
</span>
</div>