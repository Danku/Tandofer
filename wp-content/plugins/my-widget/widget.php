<?php
/*
Plugin Name: __MY-WIDGET-PLUGIN
Plugin URI: www.danku.nhely.hu
Description:  
Version: 1.0
Author: Danku Attila Zsolt
Author URI: 
License: MIT License 
*/
//[my_elso_widget]
//[my_masodik_widget]
//[my_harmadik_widget]
//[my_sidebar_widget]
//[my_front_page_sidebar_widget]
//[my_select_product]

//css
wp_enqueue_style (
    'my-widget',
    plugins_url( 'my-widget/css/my.css' ),
    array(),
    date('YmdHis')
);

//elso
function elsoFunc(){
    ob_start();
    include 'inc/elso.php';
    return ob_get_clean();
}
add_shortcode('my_elso_widget', 'elsoFunc');

//masodik
function masodikFunc(){
    ob_start();
    include 'inc/masodik.php';
    return ob_get_clean();
}
add_shortcode('my_masodik_widget', 'masodikFunc');

//harmadik
function harmadik(){
    ob_start();
    include 'inc/harmadik.php';
    return ob_get_clean();
}
add_shortcode('my_harmadik_widget', 'harmadik');

//sidebar
function sidebar(){
    ob_start();
    include 'inc/session_sidebar.php';
    return ob_get_clean();
}
add_shortcode('my_sidebar_widget', 'sidebar');

//frontPage sidebar
function frontSidebar(){
    ob_start();
    if(is_user_logged_in()){
   // include 'inc/sidebar.php';
    }
    return ob_get_clean();
}
add_shortcode('my_front_page_sidebar_widget', 'frontSidebar');

//Select product
function selectProductFunc(){
    ob_start();
    include 'inc/select.php';
    return ob_get_clean();
}

add_shortcode('my_select_product', 'selectProductFunc');



//MENU ELTÖRLÉSE//
/*
function wti_loginout_menu_link( $items, $args ) {
    if ($args->theme_location == 'primary') {
        if (is_user_logged_in()) {
            $items .= '<li id="menu-item-228" class="menu-item menu-item-type-post_type menu-item-object-page 
                            menu-item-has-children menu-item-228">
                <a href="'. site_url() .'/bejelentkezes-regisztracio/">'. __("Adatlapom szerkesztése") .'</a>                            
                        <ul class="sub-menu" style="display: none;">
                            <li id="menu-item-226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-226">
                                <a href="'.wp_logout_url().'/bejelentkezes-regisztracio">Kijelentkezés</a></li>
                        </ul>                    
                        </li>';
        } else {
            $items .= '<li id="menu-item-228" class="menu-item menu-item-type-post_type menu-item-object-page 
                            menu-item-has-children menu-item-228">
                <a href="'. site_url() .'/bejelentkezes-regisztracio/">'. __("Bejelentkezés") .'</a>                            
                        <ul class="sub-menu" style="display: none;">
                            <li id="menu-item-226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-226">
                                <a href="'.site_url().'/bejelentkezes-regisztracio">Regisztráció</a></li>
                        </ul>                    
                        </li>';
        }
    }
    return $items;
}


add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2);
*/

function kapcsolat_menu_link( $items, $args ) {
    $items .= '<li id="menu-item-229" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-229">
            <a href="'. site_url() .'/kapcsolat-felvetel/">'. __("Kapcsolat felvétel") .'</a></li>';       
    
    return $items;
}

add_filter( 'wp_nav_menu_items', 'kapcsolat_menu_link', 10, 2);



/*DOWNLOAD*/
function bookFunc(){
    ob_start();
    include 'inc/kezikonyv.php';
    return ob_get_clean();
}
add_shortcode('trafik_kezikonyv', 'bookFunc');

function downTrafikSzoft(){
    ob_start();
    include 'inc/download.php';
    return ob_get_clean();
}
add_shortcode('trafik_szoftver', 'downTrafikSzoft');