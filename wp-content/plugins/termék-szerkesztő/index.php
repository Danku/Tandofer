<?php
/*
Plugin Name: Termékek szerkesztése
Plugin URI: http://danku.hu
Description: Termék felvétel, szerkesztés, törlés.
Version: 1.0
Author: Danku Attila Zsolt
Author URI: Danku Attila Zsolt
License: MIT
Text Domain: Termék listázó
*/

// Egyéni stíluslap.
wp_enqueue_style (
    'termek_szerkeszto_style',
    plugins_url( 'termék-szerkesztő/css/my.css' ),
    array(),
    date('YmdHis')
);

function product_listing_menu(){
   add_menu_page(
    //page title
    'Termék listázó oldal',
    // menü gomb
    'Termék listázó',
    //Láthatóság
    'edit_users',
    'db_tester',
    //Funkció ami létrehozza
    'init_product_tester'
   );

}
add_action('admin_menu', 'product_listing_menu');


function init_product_tester(){
    include 'inc/listing-products.php';
}
