<?php
global $wpdb;
insertFunc();
deleteFunc();
updateFunc();
SelectUpdateFunc();
$table = 'products';
$results = $wpdb->get_results(
    "SELECT * FROM $table"
);

?>
<!--HTML-->
<div><br><br>
<table>
   <thead>
    <tr>
        <th>#</th>
        <th class="minWidth">Termék név</th>
        <th class="minWidth"s>Termék oldal id</th>
        <th>Kategória</th>
        <th>Többes</th>
        <th>Szerkesztés</th>
        <th>Törlés</th>
    </tr>
    </thead>
    <tbody>
        <?php
        foreach($results as $value){
        $id = $value->id; 
        $p_name=$value->p_name;
        $p_page_id=$value->p_page_id;
        $p_category=$value->p_category;
        $p_es=$value->p_es;
        ?>
    <tr>
        <td><?php echo $id;?></td>
        <td><?php echo $p_name;?></td>
        <td><?php echo $p_page_id;?></td>
        <td><?php switch($p_category){
            case 1: echo "Szoftverek";
                break;
            case 2: echo "Hardverek";
                break;
            case 3: echo "Szolgáltatások";
                break;
        }?></td>
        <td><?php switch($p_es){
            case 0: echo "NEM";
                break;
            case 1: echo "IGEN";
                break;
        }?></td>
        <td>
        <!--UPDATE SEND FROM-->        
        <form method="post" id="form">
            <input type="hidden" value="<?php echo $id;?>" name="update_id">            
            <input type="submit" value="SZERKESZTÉS" name="update">
        </form>
        </td>
        <td>
        <!--DELETE SEND FORM-->
        <form method="post">
            <input type="hidden" value="<?php echo $id;?>" name="delete_id">
            <input type="submit" name="delete" value="TÖRLÉS">
        </form>
        </td>
    </tr>
       <?php }?>
    </tbody>
</table>
</div><br>

<?php
//Kiválasztott termék betöltése
function selectUpdateFunc(){
    if(isset($_POST['update'])){
        global $wpdb;
        $table          = 'products';
        $p_id           = $_POST['update_id'];
        $result         = $wpdb->get_row(
            "SELECT * FROM $table WHERE id = $p_id"
        );
        $p_name         = $result->p_name;
        $p_page_id      = $result->p_page_id;
        $p_category     = $result->p_category;
        $p_es           = $result->p_es;

        ?>
        <!--UPDATE FORM-->
        <div>
        <h3 id="update-form">Termék szerkesztés</h3>
        <table id="update-table">
            <tr>
            <th>#</th>
            <th><label for="p_name_update">Termék név</label></th>
            <th><label for="p_page_id_update">Termék oldal id</label></th>
            <th><label for="p_category_update">Kategória</label></th>
            <th><label for="p_es_update">Többes</label></th>
            <th><label for="update">Szerkesztés</label></th>
            </tr>
            <tr>
            <form method="post">
            <td>
            <?php echo $p_id; ?>
            <input type="hidden" name="p_id" value="<?php echo $p_id;?>" size="2">
            </td>
            <td>
                <input type="text" name="p_name" id="p_name_update" value="<?php echo $p_name;?>" required>
            </td>
            <td>
                <input type="text" name="p_page_id" id="p_page_id_update" value="<?php echo $p_page_id;?>" required>
            </td>
            <td>
                <select name="p_category" id="p_category_update">
                    <option value="1" <?php if($p_category==1) echo "selected"; ?>>Szoftverek</option>
                    <option value="2" <?php if($p_category==2) echo "selected"; ?>>Hardverek</option>
                    <option value="3" <?php if($p_category==3) echo "selected"; ?>>Szolgáltatások</option>
                </select>
            </td>
            <td>
                <select name="p_es" id="p_es_update">
                    <option value="0" <?php if($p_es==0) echo "selected"; ?>>NEM lehet több belőlle</option>
                    <option value="1" <?php if($p_es==1) echo "selected"; ?>>Lehet több belőlle</option>
                </select>
            </td>
            <td>
                <input type="submit" name="update_products" id="update" value="SZERKESZT">
            </td>
            </form>
            <td>
                <button onclick="myUpdateHidden()">MÉGSEM</button>
            </td>
            </tr>
        </table>
        </div>
        <?php
    }
}
//Termék frissítése
function updateFunc(){
    if(isset($_POST['update_products'])){
        global $wpdb;
        $table          = 'products';
        $p_id           = $_POST['p_id'];
        $p_name         = $_POST['p_name'];
        $p_page_id      = $_POST['p_page_id'];
        $p_category     = $_POST['p_category'];
        $p_es           = $_POST['p_es'];
        //echo "        $p_id<br>$p_name<br>$p_category       ";

        $where = [ 'id' => $p_id ];
        $data = [
            'p_name' => $p_name,
            'p_page_id' => $p_page_id,
            'p_category' => $p_category,
            'p_es' => $p_es
        ];
        $update = $wpdb->update($table, $data, $where);
        if($update===false){
            echo "<br><b>Nem sikerült a Frissítés</b><br>";
        } elseif ($update == false) {
            echo "<br><b>Nem változott semmi sem!</b>";
        } else {
            echo "<br><b>Sikeresen frissült a termék!</b>";
        }

    }
}
//Termék felvétel
function insertFunc(){
    if(isset($_POST['insert'])){
        global $wpdb;
        $table          = 'products';
        $p_name         = $_POST['p_name'];
        $p_page_id      = $_POST['p_page_id'];
        $p_category     = $_POST['p_category'];
        $p_es           = $_POST['p_es'];

        $data = [
            'p_name' => $p_name,
            'p_page_id' => $p_page_id,
            'p_category' => $p_category,
            'p_es' => $p_es
        ];

        $formats        = [ '%s', '%d', '%d', '%d' ];
        
        $insert = $wpdb->insert($table, $data, $formats);
        if($insert){
            echo "<b><br>$wpdb->insert_id id-val lett beszúrva a termék!</b><br>";
        } else {
            echo "Nem sikerült a terméket hozzáadni.";
        }
    }
    ?>
        <!--INSERT FORM-->
        <div>
        <h3 style="font-size:30px;" onclick="hiddenFunc()"><a>Termék létrehozása</a></h3>
        <table id="hidden" style="display:none;">
            <tr>
            <th>#</th>
            <th><label for="p_name">Termék név</label></th>
            <th><label for="p_page_id">Termék oldal id</label></th>
            <th><label for="p_category">Kategória</label></th>
            <th><label for="p_es">Többes</label></th>
            <th><label for="insert">Létrehozás</label></th>
            </tr>
            <tr>
            <form method="post">
            <td>
                ID
            </td>
            <td>
                <input type="text" name="p_name" id="p_name" required>
            </td>
            <td>
                <input type="text" name="p_page_id" id="p_page_id" required>
            </td>
            <td>
                <select name="p_category" id="p_category">
                        <option value="1" chechked>Szoftverek</option>
                        <option value="2">Hardverek</option>
                        <option value="3">Szolgáltatások</option>
                </select>
            </td>
            <td>
                <select name="p_es" id="p_es">
                        <option value="0" checked>NEM lehet több belőlle</option>
                        <option value="1">Lehet több belőlle</option>
                </select>
            </td>
            <td>
                <input type="submit" value="LÉTREHOZ" name="insert" id="insert">
            </td>
            </form>
            <td>
                <button onclick="myInsertHidden()">MÉGSEM</button>
            </td>
            </tr>
        </table>
        </div>
    <?php
}
//Termék törlése
function deleteFunc(){
    if(isset($_POST['delete'])){
        global $wpdb;
        $table          = 'products';
        $p_id           = $_POST['delete_id'];
        $where          = [ 'id' => $p_id ];

        $delete         = $wpdb->delete($table, $where);

        if($delete){
            echo "<br><b> $p_id id-val rendelkező termék törlődött!</b>";
        } else {
            echo "<br>Nem sikerült törölni!<br>".$wpdb->last_error;
        }
    }
}
?>

<script>
    function hiddenFunc(){
        document.getElementById("hidden").style.display = "inline-block";
    }
    function myUpdateHidden(){
        document.getElementById("update-form").style.display = "none";
        document.getElementById("update-table").style.display = "none";
    }
    function myInsertHidden(){
        document.getElementById("hidden").style.display = "none";
    }
</script>