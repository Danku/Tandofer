<?php
/*
*
* Plugin Name: 	__MY JQUERY MASK
* Plugin URI:   dnaku.nhely.hu
* Description: 	phone number, date, etc.
* Version:      1.2
* Author: 		Danku Attila
* Author URI:   danku.nhely.hu
*/
// (.phone)     = +00/00 000 00 00
 
 function jQueryMaskScriptFunc() {
    wp_enqueue_script (
		'my-masked-input',
		plugins_url( 'my-jQuery-mask/js/jquery.mask.js' ),
		array(),
        date('YmdHis')
    );
    
    wp_enqueue_script (
		'my-mask-main-js',
		plugins_url( 'my-jQuery-mask/js/main.js' ),
		array('jquery'),
        date('YmdHis')
	);
}
add_action( 'wp_enqueue_scripts', 'jQueryMaskScriptFunc' );